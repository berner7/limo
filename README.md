# Übung Lineare Modelle

## Tutorial

Dieses Tutorial ist als Teil der Bachelorveranstaltung Lineare Modelle der Wirtschaftswissenschaftlichen Fakultät an der Universität Göttingen entstanden und soll Studierenden helfen die Inhalte der Vorlesung zu üben. Das Skript umfasst Möglichkeiten R-Code zu testen und einfache Quizfragen, die hoffentlich motivieren zum Selberrechnen und zur Vorbereitung der Klausur.

## Webgehostete Version

Um das Tutorial zu starten, einfach auf folgenden Button klicken
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fberner7%2Flimo.git/master?urlpath=rstudio)

Wenn das Programm das erste mal gestartet wird, werden alle nötigen Pakete in R gestartet. Dieser Vorgang kann einige Minuten dauern. Also am Besten erst mal einen Kaffee machen. Stellen Sie auch sicher, dass der Browser Pop-Up Fenster zulässt.

## Installation auf eigener Maschine

Installiere hierfür bitte erst R und Rstudio auf deiner Maschine. Hierfür kannst du einfach die Anleitung nutzen welche als Teil der .pdf-Version des Skriptes in Stud.IP hochgeladen worden ist.

Daraufhin installiere einfach das R-Package LiMo vom GWDG internen Gitlab. Nutze hierfür einfach den folgenden Befehl:
`remotes::install_gitlab(repo = "berner7/LiMo", host = "gitlab.gwdg.de")`

# Ausführen des Skriptes

Um das angebotene Skript auszufürehn einfach die folgende Befehle ausführen:
`learnr::run_tutorial("limocoaching", "LiMo")`

Nach dem ersten Ausführen des Befehls dauert es vielleicht einige Sekunden bis sich im Standard-Browsers deines Systems sich ein Tab öffnet in dem das interaktive **R** Skript ausgeführt wird.



## Probleme bei der Installation

### Fehlende Dependencies
Sollte der Befehl `remotes::install_gitlab(repo = "berner7/LiMo", host = "gitlab.gwdg.de")` nicht funktionieren, fehlt dir auf deinem Rechner das Package remotes. Hierfür einfach über den `install.packages()`-Befehl remotes nach installieren.

Nutzer von Unix-like Systemen (Linux, BSD, MacOS) müssen unter Umständen etwaige Dependencies von genutzen Packages nach installieren. Da dies allerdings eine triviale Aufgabe ist, sollte dies kein großes Problem darstellen und relativ einfach durch eine schnelle Suche im Internet zu lösen sein.

### Windows 7 Cygwin Fehler

Sollte folgender Fehler auftreten: 

` cygwin warning:`    
`MS-DOS style path detected: C:\Users\admin\AppData\Local\Temp\Rtmp0qJygg\file24403400563c.tar.gz`    
`Preferred POSIX equivalent is: /cygdrive/c/Users/admin/AppData/Local/Temp/Rtmp0qJygg/file24403400563c.tar.gz`    
`CYGWIN environment variable option "nodosfilewarning" turns off this warning.`    
`Consult the user's guide for more details about POSIX paths:`

Wenn dieser Fehler auftritt gibt es ein Problem mit dem POSIX-compatible environment Gygwin. Hierfür gibt es leider keine einfache Lösung. 

  * Die einfachste Lösungsart ist das Upgrade zu Windows 10 oder Linux basierten Betriebssystemen
  * Es ist ebefalls möglich bei der Kompilierung besondere Argumente zu übergeben welche unterumständen die Installation des packages erlauben würden. Diese Herangehensweise ist allerdings nicht immer Zielführend.
  Für mehr Information einfach dieses Web-Post lesen: https://stackoverflow.com/questions/11958083/cygwin-warning-when-building-r-package



## Verweise
Das Skript bezieht sich auf verschiedene Quellen, deren inhaltliche Aufarbeitung und Darstellung der Thematik, als Grundlage und Bezugspunkt beim Erstellen dieses Skriptes dienten. Hierbei sei vor allem auf folgende Quellen verwiesen, wobei es sich bei allen Quellen um Free-and-Open-Source Lehrinhalte der jeweiligen Autoren handelt:

*  [Einführung in R Grundlagen] von [René Kruse](https://gitlab.gwdg.de/kruse44/intro2r/).
* [Advanced R](http://adv-r.had.co.nz/) von [Hadley Wickham](http://hadley.nz/)
* [Merely Useful: Novice R](https://merely-useful.github.io/r/index.html) von [Madeleine Bonsma-Fisher et al.]()
* [R for Data Science](#https://r4ds.had.co.nz/) von [Hadley Wickham](http://hadley.nz/) und [Garrett Grolemund](https://twitter.com/statgarrett?lang=de)
* [Hands-On Programming with R](#https://rstudio-education.github.io/hopr/) von [Garrett Grolemund](https://twitter.com/statgarrett?lang=de)
* [Fundamentals of Data Visualization](#https://serialmentor.com/dataviz/) von [Claus O. Wilke](https://github.com/clauswilke)
* [YaRrr! The Pirate’s Guide to R](#https://bookdown.org/ndphillips/YaRrr/) von [Nathaniel D. Phillips](https://ndphillips.github.io/index.html)
*  Vorlesungsfolien des Statistik-Master Kurses "Introduction to Statistical Programming" und "Lineare Modelle" von [Paul Wiemann](https://www.uni-goettingen.de/de/525900.html).
